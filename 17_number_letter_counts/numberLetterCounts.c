/* numberLetterCounts
 *
 * Count the number of letters that would be written if the numbers in the series [1, 1000] were written down
 * When Hundred is being used, make the distinction between "hundred" and "hundred and"
 * Do not include hyphens or spaces in the total count
 *
 * Author: Jeremy Tobac
 *
 * Date: October 2014
 */
#include <stdio.h>
#include <stdlib.h>

#define ONES 9
#define TEENS 10
#define TENS 8
#define HUNDREDS 2
#define THOUSANDS 1
#define MAX_NAME_SIZE 20
#define ONES_IN_TENS 8
#define TENS_IN_TENS 10
#define HUNDRED_IN_HUNDREDS 9
#define ONES_IN_HUNDREDAND 99
#define ONES_AND_TENS_IN_HUNDREDS 9

#define PRINT_NUM_LETTERS 0
#define PRINT_WORDS 0

/**@brief Read in the spelling of unique names in sequenct [1-100]
 *
 *@param char **ppchrOnes: Unique names in the ones place
 *@param char **ppchrTeens: Unique names from 10-19
 *@param char **ppchrTens: Unique names in the tens place, starting from 20
 *@param char **ppchrHundreds: Unique namese in hundreds palce (includes "hundred" and "hundredand"
 *@param char **ppchrThousands: Unique names in the thousands place
 *
 *@ret Returns 0 upon success; -1 on failure
 */
int read_digit_file(char **ppchrOnes, char **ppchrTeens, char **ppchrTens, char **ppchrHundreds, char **ppchrThousands)
{
	char chrTemp;
	int intCount, intCharCount;
	FILE *pfIn;

	intCount = intCharCount = 0;

	pfIn = fopen("numbers.txt", "r");
	if(!pfIn){
		fprintf(stderr, "Could not open input file\n");
		return -1;
	}

	while((chrTemp = fgetc(pfIn)) != EOF){
		
		if(chrTemp == '\n'){
			chrTemp = '\0';
		}
		if(intCount < ONES){
			ppchrOnes[intCount][intCharCount] = chrTemp;
		}
		else if(intCount < (TEENS + ONES)){
			ppchrTeens[intCount - (ONES)][intCharCount] = chrTemp;
		} 
		else if(intCount < (TENS + TEENS + ONES)){
			ppchrTens[intCount - (TEENS + ONES)][intCharCount] = chrTemp;
			
		}
		else if(intCount < (HUNDREDS + TENS + TEENS + ONES)){
			ppchrHundreds[intCount - (TENS + TEENS + ONES)][intCharCount] = chrTemp;
		}
		else if(intCount < (THOUSANDS + HUNDREDS + TENS + TEENS + ONES)){
			ppchrThousands[intCount - (HUNDREDS + TENS + TEENS + ONES)][intCharCount] = chrTemp;
		}

		intCharCount++;
		if(chrTemp == '\0'){
			intCount++;
			intCharCount = 0;
		}
	}

	fclose(pfIn);
	
	return 0;
}

/**@brief Print out the individual unique words in the char arrays from 1-100
 *
 *@param char **ppchrOnes: Unique names in the ones place
 *@param char **ppchrTeens: Unique names from 10-19
 *@param char **ppchrTens: Unique names in the tens place, starting from 20
 *@param char **ppchrHundreds: Unique namese in hundreds palce (includes "hundred" and "hundredand"
 *@param char **ppchrThousands: Unique names in the thousands place
 */
void print_words(char **ppchrOnes, char **ppchrTeens, char **ppchrTens, char **ppchrHundreds, char **ppchrThousands)
{

	int intI;
	
	for(intI=0; intI < ONES; intI++){
		printf("%s, ", ppchrOnes[intI]);
	}
	printf("\n");
	for(intI=0; intI < TEENS; intI++){
		printf("%s, ", ppchrTeens[intI]);
	}
	printf("\n");
	for(intI=0; intI < TENS; intI++){
		printf("%s, ", ppchrTens[intI]);
	}
	printf("\n");
	for(intI=0; intI < HUNDREDS; intI++){
		printf("%s, ", ppchrHundreds[intI]);
	}
	printf("\n");
	for(intI=0; intI < THOUSANDS; intI++){
		printf("%s, ", ppchrThousands[intI]);
	}
	printf("\n");

}

/**@brief Find the total number of letters in each array representing the unique names for all the digits
 * from [1,1000]
 *
 *@param char **ppchrOnes: Unique names in the ones place
 *@param char **ppchrTeens: Unique names from 10-19
 *@param char **ppchrTens: Unique names in the tens place, starting from 20
 *@param char **ppchrHundreds: Unique namese in hundreds palce (includes "hundred" and "hundredand"
 *@param char **ppchrThousands: Unique names in the thousands place
 *@param int **pintOnes: Number of letters in the unique names in the ones place
 *@param int **pintTeens: Number of letters int the unique names from 10-19
 *@param int **pintTens: Number of letters int the unique names in the tens place, starting from 20
 *@param int **pintHundreds: Number of letters int the unique namese in hundreds palce (includes "hundred" and "hundredand"
 *@param int **pintThousands: Number of letters int the unique names in the thousands place
 */
void num_letters_in_words(char **ppchrOnes, char **ppchrTeens, char **ppchrTens, char **ppchrHundreds, char **ppchrThousands, int *pintOnes, int* pintTeens, int* pintTens, int *pintHundreds, int *pintThousands)
{

	int intLetters, intI;

	for(intI = 0; intI < ONES; intI++){
		for(intLetters = 0; ppchrOnes[intI][intLetters] != '\0'; intLetters++){
			;
		}
		pintOnes[intI] = intLetters;
	}

	for(intI = 0; intI < TEENS; intI++){
		for(intLetters = 0; ppchrTeens[intI][intLetters] != '\0'; intLetters++){
			;
		}
		pintTeens[intI] = intLetters;
	}

	for(intI = 0; intI < TENS; intI++){
		for(intLetters = 0; ppchrTens[intI][intLetters] != '\0'; intLetters++){
			;
		}
		pintTens[intI] = intLetters;
	}
	
	for(intI = 0; intI < HUNDREDS; intI++){
		for(intLetters = 0; ppchrHundreds[intI][intLetters] != '\0'; intLetters++){
			;
		}
		pintHundreds[intI] = intLetters;
	}

	for(intI = 0; intI < THOUSANDS; intI++){
		for(intLetters = 0; ppchrThousands[intI][intLetters] != '\0'; intLetters++){
			;
		}
		pintThousands[intI] = intLetters;
	}
	

}

/**@brief Print the number of letters of the unique words in the series [1,1000]
 *
 *Print the number of letters of the unique words in the series [1,1000], eg "one" has 3 letters
 *
 *@param int **pintOnes: Number of letters in the unique names in the ones place
 *@param int **pintTeens: Number of letters int the unique names from 10-19
 *@param int **pintTens: Number of letters int the unique names in the tens place, starting from 20
 *@param int **pintHundreds: Number of letters int the unique namese in hundreds palce (includes "hundred" and "hundredand"
 *@param int **pintThousands: Number of letters int the unique names in the thousands place
 */ 
void print_num_letters(int *pintOnes, int *pintTeens, int *pintTens, int *pintHundreds, int *pintThousands)
{

	int intI;

	for(intI = 0; intI < ONES; intI++){
		printf("%d ", pintOnes[intI]);
	}
	printf("\n");
	for(intI = 0; intI < TEENS; intI++){
		printf("%d ", pintTeens[intI]);
	}
	printf("\n");
	for(intI = 0; intI < TENS; intI++){
		printf("%d ", pintTens[intI]);
	}
	printf("\n");
	for(intI = 0; intI < HUNDREDS; intI++){
		printf("%d ", pintHundreds[intI]);
	}
	printf("\n");
	for(intI = 0; intI < THOUSANDS; intI++){
		printf("%d ", pintThousands[intI]);
	}
	printf("\n");

}

/**brief Count the total number of letters in the series [1, 1000]
 *
 * Count the total number of letters in the series [1, 1000]
 * Find the total number of letters from [1,9][10,19][20,99][100,999][1000] and add them all together
 * There is a difference between 100 (the and word) "one hundred" and 101 "one hundred AND one"
 *
 *@param int **pintOnes: Number of letters in the unique names in the ones place
 *@param int **pintTeens: Number of letters int the unique names from 10-19
 *@param int **pintTens: Number of letters int the unique names in the tens place, starting from 20
 *@param int **pintHundreds: Number of letters int the unique namese in hundreds palce (includes "hundred" and "hundredand"
 *
 *@ret Returns the total number of letters in the sequence [1, 1000]
 */
unsigned int count(int *pintOnes, int *pintTeens, int *pintTens, int *pintHundreds, int *pintThousands)
{
	
	int intI;
	unsigned int uintTotalLetters, uintOnes, uintTeens, uintTens, uintHundreds, uintThousands;
	unsigned int uint1_9, uint10_19, uint20_99, uint100_999, uint1000;

	uintTotalLetters = uintOnes = uintTeens = uintTens = uintHundreds = uintThousands = 0;
	
	for(intI=0; intI < ONES; intI++){
		uintOnes += (unsigned int)pintOnes[intI];
	}
	for(intI=0; intI < TEENS; intI++){
		uintTeens += (unsigned int)pintTeens[intI];
	}
	for(intI=0; intI < TENS; intI++){
		uintTens += (unsigned int)pintTens[intI];
	}
	for(intI=0; intI < THOUSANDS; intI++){
		uintThousands += (unsigned int)pintThousands[intI];
	}

	uint1_9 = uintOnes;
	uint10_19 = uintTeens;
	uint20_99 = (uintTens * TENS_IN_TENS) + (uintOnes * ONES_IN_TENS);
	uint100_999 = (((unsigned int)pintHundreds[0] * HUNDRED_IN_HUNDREDS) + uint1_9) +((((unsigned int)pintHundreds[1] * HUNDRED_IN_HUNDREDS) + uint1_9)*ONES_IN_HUNDREDAND) + ((uint1_9 + uint10_19 + uint20_99) * ONES_AND_TENS_IN_HUNDREDS);

	uint1000 = uintTotalLetters + uintThousands + (unsigned int)pintOnes[0];

	uintTotalLetters = uint1_9 + uint10_19 + uint20_99 + uint100_999 + uint1000;

	return uintTotalLetters;

}

int main(){

	int intI, intRet;
	int *pintOnes, *pintTeens, *pintTens, *pintHundreds, *pintThousands;
	char **ppchrOnes, **ppchrTeens, **ppchrTens, **ppchrHundreds, **ppchrThousands;
	unsigned int uintTotalLetters = 0;
	
	ppchrOnes=(char **)malloc(sizeof(char*)*ONES);
	if(ppchrOnes == NULL){
		fprintf(stderr, "Could not malloc space for ppchrOnes\n");
		return -1;
	}
	for(intI = 0; intI <  ONES; intI++){
		ppchrOnes[intI]=(char*)malloc(sizeof(char)*MAX_NAME_SIZE);
		if(ppchrOnes[intI] == NULL){
			fprintf(stderr, "Could not malloc space for ppchrOnes[%d]\n", intI);
			return -1;
		}
	}

	ppchrTeens=(char **)malloc(sizeof(char*)*TEENS);
	if(ppchrTeens == NULL){
		fprintf(stderr, "Could not malloc space for ppchrTeens\n");
		return -1;
	}
	for(intI = 0; intI <  TEENS; intI++){
		ppchrTeens[intI]=(char*)malloc(sizeof(char)*MAX_NAME_SIZE);
		if(ppchrTeens[intI] == NULL){
			fprintf(stderr, "Could not malloc space for ppchrTeens[%d]\n", intI);
			return -1;
		}
	}

	ppchrTens=(char **)malloc(sizeof(char*)*TENS);
	if(ppchrTens == NULL){
		fprintf(stderr, "Could not malloc space for ppchrTens\n");
		return -1;
	}
	for(intI = 0; intI <  TENS; intI++){
		ppchrTens[intI]=(char*)malloc(sizeof(char)*MAX_NAME_SIZE);
		if(ppchrTens[intI] == NULL){
			fprintf(stderr, "Could not malloc space for ppchrTens[%d]\n", intI);
			return -1;
		}
	}

	ppchrHundreds=(char **)malloc(sizeof(char*)*HUNDREDS);
	if(ppchrHundreds == NULL){
		fprintf(stderr, "Could not malloc space for ppchrHundreds\n");
		return -1;
	}
	for(intI = 0; intI <  HUNDREDS; intI++){
		ppchrHundreds[intI]=(char*)malloc(sizeof(char)*MAX_NAME_SIZE);
		if(ppchrHundreds[intI] == NULL){
			fprintf(stderr, "Could not malloc space for ppchrHundreds[%d]\n", intI);
			return -1;
		}
	}

	ppchrThousands=(char **)malloc(sizeof(char*)*THOUSANDS);
	if(ppchrThousands == NULL){
		fprintf(stderr, "Could not malloc space for ppchrThousands\n");
		return -1;
	}
	for(intI = 0; intI <  THOUSANDS; intI++){
		ppchrThousands[intI]=(char*)malloc(sizeof(char)*MAX_NAME_SIZE);
		if(ppchrThousands[intI] == NULL){
			fprintf(stderr, "Could not malloc space for ppchrThousands[%d]\n", intI);
			return -1;
		}
	}

	pintOnes=(int*)malloc(sizeof(int)*ONES);
	if(pintOnes == NULL){
		fprintf(stderr, "Could not malloc space for pintOnes\n");
		return -1;
	}

	pintTeens=(int*)malloc(sizeof(int)*TEENS);
	if(pintTeens == NULL){
		fprintf(stderr, "Could not malloc space for pintTeens\n");
		return -1;
	}

	pintTens=(int*)malloc(sizeof(int)*TENS);
	if(pintTens == NULL){
		fprintf(stderr, "Could not malloc space for pintTens\n");
		return -1;
	}

	pintHundreds=(int*)malloc(sizeof(int)*HUNDREDS);
	if(pintHundreds == NULL){
		fprintf(stderr, "Could not malloc space for pintHundreds\n");
		return -1;
	}

	pintThousands=(int*)malloc(sizeof(int)*THOUSANDS);
	if(pintThousands == NULL){
		fprintf(stderr, "Could not malloc space for pintThousands\n");
		return -1;
	}

	intRet = read_digit_file(ppchrOnes, ppchrTeens, ppchrTens, ppchrHundreds, ppchrThousands);
	if(intRet){
		return intRet;
	}

#if PRINT_WORDS	
	print_words(ppchrOnes, ppchrTeens, ppchrTens, ppchrHundreds, ppchrThousands);
#endif
	num_letters_in_words(ppchrOnes, ppchrTeens, ppchrTens, ppchrHundreds, ppchrThousands, pintOnes, pintTeens, pintTens, pintHundreds, pintThousands);

#if PRINT_NUM_LETTERS
	print_num_letters(pintOnes, pintTeens, pintTens, pintHundreds, pintThousands);
#endif

	uintTotalLetters = count(pintOnes, pintTeens, pintTens, pintHundreds, pintThousands);

	printf("Total number of letters counting from one to one thousand is : %u\n", uintTotalLetters);

	free(pintOnes);
	pintOnes = NULL;
	if(pintOnes){
		fprintf(stderr, "Error freeing pintOnes\n");
		return -1;
	}
	free(pintTeens);
	pintTeens = NULL;
	if(pintTeens){
		fprintf(stderr, "Error freeing pintTeens\n");
		return -1;
	}
	free(pintTens);
	pintTens = NULL;
	if(pintTens){
		fprintf(stderr, "Error freeing pintTens\n");
		return -1;
	}
	free(pintHundreds);
	pintHundreds = NULL;
	if(pintHundreds){
		fprintf(stderr, "Error freeing pintHundreds\n");
		return -1;
	}
	free(pintThousands);
	pintThousands = NULL;
	if(pintThousands){
		fprintf(stderr, "Error freeing pintThousands\n");
		return -1;
	}

	free(ppchrOnes);
	ppchrOnes = NULL;
	if(ppchrOnes){
		fprintf(stderr, "Error freeing ppchrOnes\n");
		return -1;
	}
	free(ppchrTeens);
	ppchrTeens = NULL;
	if(ppchrTeens){
		fprintf(stderr, "Error freeing ppchrTeens\n");
		return -1;
	}
	free(ppchrTens);
	ppchrTens = NULL;
	if(ppchrTens){
		fprintf(stderr, "Error freeing ppchrTens\n");
		return -1;
	}
	free(ppchrHundreds);
	ppchrHundreds = NULL;
	if(ppchrHundreds){
		fprintf(stderr, "Error freeing ppchrHundreds\n");
		return -1;
	}
	free(ppchrThousands);
	ppchrThousands = NULL;
	if(ppchrThousands){
		fprintf(stderr, "Error freeing ppchrThousands\n");
		return -1;
	}

	return 0;
}
